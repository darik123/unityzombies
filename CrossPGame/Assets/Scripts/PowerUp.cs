﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour
{
	//public GameObject g;
	//public float respTime;
	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	// Don't destroy it, just don't render
	IEnumerator Respawn ()
	{
		this.renderer.enabled = false;
		this.collider.enabled = false;
		yield return new WaitForSeconds (4);
		this.renderer.enabled = true;
		this.collider.enabled = true;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.collider.gameObject.tag == "Player" || other.collider.gameObject.tag == "Zombie") {
			StartCoroutine (Respawn ());
		}
	}
}
