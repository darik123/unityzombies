﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	public float speed = 5.0f;
	public float mouseSens = 5.0f;
	//public float sprint = 10.0f;
	public int health = 100;
	public int score = 0;
	public float canRotate = 60.0f;
	public LayerMask ignoreLayers;
	public Text scoreText;
	public Text healthT;
	public Text winT;
	public Text loseT;
	public AudioClip audio;
	Touch touch;
	RuntimePlatform platform = Application.platform;
	//public Text sprintT;

	
	private float rotUD = 0.0f;
	private float rotLR = 0.0f;
	private float restartTimer = 0.0f;
	float hInput = 0.0f;
	float fbInput = 0.0f;
	float upInput = 0.0f;
	float rotInput = 0.0f;

	//public SphereCollider sc = new SphereCollider ();

	// Use this for initialization
	void Start ()
	{
		healthT.text = "";
		loseT.text = "LOSER";
		loseT.enabled = false;
		winT.text = "WINNER";
		winT.enabled = false;
		health = 100;
		UpdateScore ();
		UpdateHealth ();
		Screen.lockCursor = true;
		restartTimer = 0.0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		restartTimer += Time.deltaTime;
		if (restartTimer > 100) {
			Debug.Log ("WINNER");
			winT.enabled = true;
			Application.LoadLevel (Application.loadedLevel);
		}

		//		if (platform == RuntimePlatform.WindowsEditor) {}
		// Movement
		#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			// && sprint != 0) {
			speed = 15.0f;
			if (speed > 15.0f) {
				speed = 15.0f;
			}
			//sprint--;
			//UpdateSprint ();
		} else if ((Input.GetKeyUp (KeyCode.LeftShift))) {
			speed = 5.0f;
		}

		float speedLR = Input.GetAxis ("Horizontal") * speed;
		float speedFB = Input.GetAxis ("Vertical") * speed;

		rotLR = Input.GetAxis ("Mouse X") * mouseSens;
		rotUD -= Input.GetAxis ("Mouse Y") * mouseSens;
		// Limit the mouse movement
		//rotLR = Mathf.Clamp(rotLR,-200, 200);
		rotUD = Mathf.Clamp (rotUD, -canRotate, canRotate);

		transform.Rotate (0, rotLR, 0);
		Camera.main.transform.localRotation = Quaternion.Euler (rotUD, 0, 0);

		Vector3 vSpeed = new Vector3 (speedLR, -10, speedFB);

		Debug.Log (vSpeed.y);
			
		CharacterController cc = GetComponent<CharacterController> ();
			
		vSpeed = transform.rotation * vSpeed;
			
		cc.Move (vSpeed * Time.deltaTime);

		// Check if Windows
		//		if (platform == RuntimePlatform.WindowsEditor) {}
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, ignoreLayers)) {
				print (hit.collider.tag);
				if (hit.collider.gameObject.tag == "Zombie") {
					if (!hit.collider.animation.IsPlaying ("back_fall")) {
						hit.collider.animation.Play ("back_fall");
						GameObject g = GameObject.FindGameObjectWithTag ("Zombie");
						Enemy e = g.GetComponent<Enemy> ();
						e.speed = 0.0f;
						e.gameObject.transform.Translate (new Vector3 ());
						Destroy (hit.collider.gameObject, 0.5f);
						AddScore (20);
					}
				}
			}
			
		}
#else
		MoveX(hInput);
		MoveZ(fbInput);
		MoveCamY(rotInput);
		MoveCamP(upInput);
		
		CharacterController cc = GetComponent<CharacterController>();
		cc.Move(new Vector3(0, -10 * Time.deltaTime, 0));

		//ShootMobile();
#endif

		// if Android Accelerometer
//		if (platform == RuntimePlatform.Android) {
//			transform.Translate (Input.acceleration.x, 0, -Input.acceleration.z);
//		}
//			if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
//			{
//				Vector2 touchDeltaPos = Input.GetTouch(0).deltaPosition;
//
//				transform.Translate(-touchDeltaPos.x * 0.5f, -touchDeltaPos.y * 0.5f, 0);
//			}
//		}
	

		// Dead on fall of level
		if (health <= 0.0f || this.transform.localPosition.y <= -10) {
			Debug.Log ("GameOver");
			loseT.enabled = true;
			if (restartTimer >= 1) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}

	}

	void MoveX (float horizontal)
	{
		Vector3 vec = new Vector3 ();
		vec.x = horizontal * this.speed * Time.deltaTime;
		CharacterController a = this.GetComponent<CharacterController> ();
		vec = this.transform.rotation * vec;
		a.Move (vec);
	}

	void MoveZ (float forwardBack)
	{
		Vector3 vec = new Vector3 ();
		vec.z = forwardBack * this.speed * Time.deltaTime;
		CharacterController a = this.GetComponent<CharacterController> ();
		vec = this.transform.rotation * vec;
		a.Move (vec);
	}

	public void StartMovingX (float horizontal)
	{
		hInput = horizontal;
	}

	public void StartMovingZ (float forwardBack)
	{
		fbInput = forwardBack;
	}

	void MoveCamP (float pitch)
	{	
		float canRot = 60.0f;
		Quaternion q = new Quaternion ();
		Vector3 vec = new Vector3 ();
		//q.x -= pitch * 5.0f;
		vec.x = pitch * 20.0f;
		//Camera.main.transform.Rotate(vec);
		Camera.main.transform.localRotation = Quaternion.Euler (vec);
	}

	void MoveCamY (float yaw)
	{
		Vector3 vec = new Vector3 ();
		vec.y = yaw * mouseSens * Time.deltaTime;
		this.transform.Rotate (vec);
	}

	public void StartMovingCamP (float pitch)
	{
		upInput = pitch;
	}

	public void StartMovingCamY (float yaw)
	{
		rotInput = yaw;
	}

	public void ShootMobile ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, ignoreLayers)) {
				print (hit.collider.tag);
				if (hit.collider.gameObject.tag == "Zombie") {
					if (!hit.collider.animation.IsPlaying ("back_fall")) {
						hit.collider.animation.Play ("back_fall");
						GameObject g = GameObject.FindGameObjectWithTag ("Zombie");
						Enemy e = g.GetComponent<Enemy> ();
						e.speed = 0.0f;
						e.gameObject.transform.Translate (new Vector3 ());
						Destroy (hit.collider.gameObject, 0.5f);
						AddScore (20);
					}
				}
			}
		
		}
	}
	
	//	void OnControllerColliderHit(ControllerColliderHit hit)
//	{
//		if(hit.gameObject.tag == "Zombie")
//		{
//			Debug.Log("HitZombie");
////			health -= 20;
////			UpdateHealth();
//		}
//	}
	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.tag == "Zombie") {

			health -= 20;
			UpdateHealth ();
			//e.animation.Play ("attack");
			//Destroy (col.gameObject, 1);
			Debug.Log ("HEY");
			Camera.main.audio.PlayOneShot (audio);
		}
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.collider.tag == "PickUp") {
			if (health < 100) {
				health += 50;
				if (health >= 100) {
					health = 100;
				}
				UpdateHealth ();
			}
		}
	}

	public void AddScore (int newScore)
	{
		score += newScore;
		UpdateScore ();
	}

	void UpdateScore ()
	{
		scoreText.text = "Score is: " + score.ToString ();
	}

	void UpdateHealth ()
	{
		healthT.text = "Health is: " + health.ToString ();
	}
	

//	void UpdateSprint ()
//	{
//		sprintT.text = "Sprint left: " + sprint.ToString ();
//	}
}