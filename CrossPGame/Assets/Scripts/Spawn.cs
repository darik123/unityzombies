﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{

	public GameObject zombie;
	public Vector3 vSpawn = new Vector3 ();
	public int zombieCount = 0;
	public float spawnWait = 0.0f;
	public float startWait = 0.0f;
	public float waveWait = 0.0f;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (SpawnWaves ());
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	// Spawn waves of enemies
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		int a = 0;
		while (a < 1000) {
			for (int i = 0; i < zombieCount; i++) {
				Vector3 spawnPos = new Vector3 (Random.Range (-vSpawn.x, vSpawn.x), vSpawn.y, vSpawn.z);
				Quaternion spawnRot = this.transform.rotation;
				Instantiate (zombie, spawnPos, spawnRot);
				yield return new WaitForSeconds (spawnWait);
			}
			++a;
			yield return new WaitForSeconds(waveWait);
		}
	}
}
