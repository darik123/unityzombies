﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour
{
	public GameObject g;
	public bool destroyed = false;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (Spawn ());
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	// Spawn the PickUp
	IEnumerator Spawn ()
	{
		if(destroyed == true){
			Vector3 spawnPos = this.transform.position;
			Quaternion spawnRot = Quaternion.identity;
			Instantiate (g, spawnPos, spawnRot);
			destroyed = false;
		}
		yield return new WaitForSeconds(1);
	}
}
