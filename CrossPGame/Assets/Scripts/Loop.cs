﻿using UnityEngine;
using System.Collections;


public class Loop : MonoBehaviour {
	
	public float dur = 0.0f;
	public Light light;


	// Use this for initialization
	void Start () {
		light = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		float a = Time.time / dur * 2 * Mathf.PI;
		float b = Mathf.Cos (a) * 0.5f + 0.5f;
		light.intensity = b;
	}
}
