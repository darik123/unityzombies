﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{

	public AudioSource source;

	public float speed = 0.0f;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (Vector3.forward * speed * Time.deltaTime);
		if(transform.position.y <= -5.0f)
		{
			Destroy(this.gameObject, 1.0f);
		}
		//transform.Translate(Vector3.up * Time.deltaTime, Space.World);
	}

	
	void OnCollisionEnter (Collision col)
	{
		if (col.gameObject.name == "Player") {
			//this.transform.LookAt(col.gameObject.transform.position);
			speed = 0.0f;
			this.transform.Translate(new Vector3());
			this.animation.Play ("attack");
			Destroy (this.gameObject, 1.0f);
			Debug.Log ("HEY");
		}
    }
}
